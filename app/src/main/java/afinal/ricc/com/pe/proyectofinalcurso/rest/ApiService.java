package afinal.ricc.com.pe.proyectofinalcurso.rest;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by riccMP on 04/10/2018.
 */
public interface ApiService {


    /* Metodos WS */

    @GET("syncSedes")
    Call<ClinicaResponse> syncClinica();


}
