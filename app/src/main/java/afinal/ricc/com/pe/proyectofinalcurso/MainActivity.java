package afinal.ricc.com.pe.proyectofinalcurso;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Arrays;
import java.util.List;

import afinal.ricc.com.pe.proyectofinalcurso.adapter.SedeAdapter;
import afinal.ricc.com.pe.proyectofinalcurso.model.Sede;
import afinal.ricc.com.pe.proyectofinalcurso.rest.ClinicaResponse;
import afinal.ricc.com.pe.proyectofinalcurso.util.Constants;
import afinal.ricc.com.pe.proyectofinalcurso.util.FunctionsUtils;
import afinal.ricc.com.pe.proyectofinalcurso.util.NetworkConstants;
import afinal.ricc.com.pe.proyectofinalcurso.viewmodel.SedeViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private RecyclerView rviSedes;
    private SwipeRefreshLayout sreSede;

    private SedeAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private SedeViewModel vmSede;


    //Dialog Sede
    private TextInputEditText eteName;
    private TextInputEditText eteAddress;
    private TextInputEditText eteLatitude;
    private TextInputEditText eteLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        rviSedes = findViewById(R.id.rviSedes);
        sreSede = findViewById(R.id.sreSedes);

        sreSede.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refrescarSedes();
            }
        });

        vmSede = ViewModelProviders.of(this).get(SedeViewModel.class);


        configurarClinicas();

        if (isConnected())
            sincronizarSedes();
        else
            consultarSedesBD();

    }


    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_list, menu);

        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_sede:
                showDialogCreateSede();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void configurarClinicas() {
        adapter = new SedeAdapter(this);
        layoutManager = new LinearLayoutManager(this);

        rviSedes.setHasFixedSize(true);
        rviSedes.setLayoutManager(layoutManager);
        rviSedes.setAdapter(adapter);
    }


    public void mostrarLoader(final boolean showLoader) {
        sreSede.post(new Runnable() {
            @Override
            public void run() {
                sreSede.setRefreshing(showLoader);
            }
        });
    }

    private void consultarSedesBD() {
        vmSede.getAllSedes().observe(MainActivity.this, new Observer<List<Sede>>() {
            @Override
            public void onChanged(List<Sede> lstSede) {

                adapter.setList(lstSede);

            }
        });
    }

    private void sincronizarSedes() {

        mostrarLoader(true);

        //Instanciamos clase call
        Call<ClinicaResponse> syncSedeResponse = apiService.syncClinica();

        //Consumir servicio
        syncSedeResponse.enqueue(new Callback<ClinicaResponse>() {
            @Override
            public void onResponse(Call<ClinicaResponse> call, Response<ClinicaResponse> response) {

                //Verificar que la peticion sea correcta
                String msgResultado = response.message();


                if (msgResultado.equals(Constants.RESPONSE_OK_GET)) {

                    List<Sede> lstSedes = response.body().getSedeList();

                    // Actualizamos el recycler view
                    adapter.addAll(lstSedes);

                    //Insertamos la información a la base de datos
                    vmSede.insertSedes(lstSedes);

                    mostrarLoader(false);


                } else {
                    String networkResponse = FunctionsUtils.validateNetworkResponse(response.raw().code(), " las Clinicas ", NetworkConstants.HTTP_GET);
                    Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                }

                mostrarLoader(false);

            }

            @Override
            public void onFailure(Call<ClinicaResponse> call, Throwable t) {
                String networkResponse = FunctionsUtils.validateNetworkResponse(NetworkConstants.TEMP_REDIRECT, " las Clinicas ", NetworkConstants.HTTP_GET);

                Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                mostrarLoader(false);

            }
        });

    }


    private void refrescarSedes() {

        mostrarLoader(true);
        //Instanciamos clase call
        Call<ClinicaResponse> syncSedeResponse = apiService.syncClinica();

        //Consumir servicio
        syncSedeResponse.enqueue(new Callback<ClinicaResponse>() {
            @Override
            public void onResponse(Call<ClinicaResponse> call, Response<ClinicaResponse> response) {

                //Verificar que la peticion sea correcta
                String msgResultado = response.message();


                if (msgResultado.equals(Constants.RESPONSE_OK_GET)) {

                    List<Sede> lstSedes = response.body().getSedeList();

                    lstSedes.get(2).setNombreSede("Clinica Modificada, se almacena en la BD.");
                    lstSedes.get(4).setNombreSede("Clinica Modificada, revisar el cambio.");

                    // Actualizamos el recycler view
                    adapter.setList(lstSedes);

                    //Actualizamos la información a la base de datos
                    vmSede.updateSedes(lstSedes);


                } else {
                    String networkResponse = FunctionsUtils.validateNetworkResponse(response.raw().code(), " las Clinicas ", NetworkConstants.HTTP_GET);
                    Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                }

                mostrarLoader(false);

            }

            @Override
            public void onFailure(Call<ClinicaResponse> call, Throwable t) {
                String networkResponse = FunctionsUtils.validateNetworkResponse(NetworkConstants.TEMP_REDIRECT, " las Clinicas ", NetworkConstants.HTTP_GET);

                Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                mostrarLoader(false);

            }
        });

    }

    //region Dialog

    private void showDialogCreateSede() {
        MaterialDialog dialog =
                new MaterialDialog.Builder(this)
                        .title(R.string.dialog_client_create)
                        .customView(R.layout.dialog_creation_sede, true)
                        .positiveText(R.string.general_create)
                        .negativeText(R.string.general_cancel)
                        .autoDismiss(false)
                        .cancelable(false)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                if (isValid()) {
                                    String nameSede = eteName.getText().toString();
                                    String addressSede = eteAddress.getText().toString();
                                    String latitudeSede = eteLatitude.getText().toString();
                                    String longitudeSede = eteLongitude.getText().toString();

                                    //Instanciamos un objecto sede con los valores obtenidos del dialog y lo creamos en la BD

                                    Sede sede = new Sede();
                                    //vmSede.insertSede(sede);
                                    dialog.dismiss();
                                }

                            }
                        })
                        .build();

        eteName = dialog.getCustomView().findViewById(R.id.eteName);
        eteAddress = dialog.getCustomView().findViewById(R.id.eteAddress);
        eteLatitude = dialog.getCustomView().findViewById(R.id.eteLatitude);
        eteLongitude = dialog.getCustomView().findViewById(R.id.eteLongitude);


        dialog.show();
    }

    public boolean isValid() {

        boolean isValid = FunctionsUtils.EmptyValidate(eteName, "Nombre no puede ser vacío.");


        if (isValid)
            isValid = FunctionsUtils.EmptyValidate(eteAddress, "Dirección no puede ser vacío.");

        if (isValid)
            isValid = FunctionsUtils.EmptyValidate(eteLatitude, "Latitud no puede ser vacío.");

        if (isValid)
            isValid = FunctionsUtils.EmptyValidate(eteLongitude, "Longitud no puede ser vacío.");


        return isValid;
    }
    //endregion


}
