package afinal.ricc.com.pe.proyectofinalcurso.rest;

import java.util.List;

import afinal.ricc.com.pe.proyectofinalcurso.model.Sede;
import afinal.ricc.com.pe.proyectofinalcurso.model.SedePrevencion;


/**
 * Created by riccMP on 04/10/2018.
 */

public class ClinicaResponse {

    private List<Sede> sedeList;
    private List<SedePrevencion> prevencionList;



    public List<Sede> getSedeList() {
        return sedeList;
    }

    public void setSedeList(List<Sede> sedeList) {
        this.sedeList = sedeList;
    }

    public List<SedePrevencion> getPrevencionList() {
        return prevencionList;
    }

    public void setPrevencionList(List<SedePrevencion> prevencionList) {
        this.prevencionList = prevencionList;
    }


}
