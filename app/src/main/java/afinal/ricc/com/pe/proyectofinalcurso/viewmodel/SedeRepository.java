package afinal.ricc.com.pe.proyectofinalcurso.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import afinal.ricc.com.pe.proyectofinalcurso.db.AppDataBase;
import afinal.ricc.com.pe.proyectofinalcurso.db.dao.SedeDAO;
import afinal.ricc.com.pe.proyectofinalcurso.model.Sede;


public class SedeRepository {
    AppDataBase db;
    private SedeDAO mSedeDAO;
    private ExecutorService executorService;

    SedeRepository(Application application) {
        db = AppDataBase.getAppDb(application);

        mSedeDAO = db.sedeDAO();
        executorService = Executors.newSingleThreadExecutor();

    }


    LiveData<List<Sede>> getAllSedes() {
        return mSedeDAO.listAll();
    }


    public void insertSedes(final List<Sede> lstSede) {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                mSedeDAO.deleteAll();
                mSedeDAO.insertSedes(lstSede);

            }
        });
    }

    public void updateSedes(final List<Sede> lstSede) {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                mSedeDAO.updateSedes(lstSede);
            }
        });
    }


}
