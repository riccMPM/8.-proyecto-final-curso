package afinal.ricc.com.pe.proyectofinalcurso.rest;

import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import afinal.ricc.com.pe.proyectofinalcurso.util.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by riccMP on 04/10/2018.
 */
public class RestClient {

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private ApiService apiService;
    private Retrofit retrofit;
    private static HashSet<String> cookies = null;

    public RestClient(String username, String password) {
        if (username != null && password != null) {
            createService( username, password);
        } else {
            createService();
        }
    }

    public void createService() {
        createService(null, null);
    }

    public void createService(String username, String password) {
        if (username != null && password != null) {
            String credentials = username + ":" + password;
            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    if(cookies!=null){
                        for (String cookie : cookies) {
                            requestBuilder.addHeader("Cookie", cookie);
                        }
                    }
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());

                    if (!originalResponse.headers("Set-Cookie").isEmpty()) {
                        HashSet<String> objCookies = new HashSet<>();

                        for (String header : originalResponse.headers("Set-Cookie")) {
                            objCookies.add(header);
                        }
                        cookies = objCookies;
                    }

                    return originalResponse;
                }
            });
        }

        httpClient.readTimeout(Constants.REST_TIMEOUT, TimeUnit.SECONDS);
        httpClient.connectTimeout(Constants.REST_TIMEOUT, TimeUnit.SECONDS);
        httpClient.writeTimeout(Constants.REST_TIMEOUT, TimeUnit.SECONDS);

        OkHttpClient client = httpClient.build();

         retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(getGsonConverterFactory())
                .client(client)
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }

    public GsonConverterFactory getGsonConverterFactory() {
        Gson gson = new GsonBuilder()
                .create();

        return GsonConverterFactory.create(gson);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }


}
