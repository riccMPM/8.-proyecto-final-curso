package afinal.ricc.com.pe.proyectofinalcurso.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import afinal.ricc.com.pe.proyectofinalcurso.util.Constants;

@Entity(tableName = Constants.NAME_TABLE_SEDE_PREVENCION)
public class SedePrevencion {

    @PrimaryKey()
    private int idSedePrevencion;
    private Long idSede;
    private String nombre;
    private String latitud;
    private String longitud;

    public SedePrevencion() {
    }


    public SedePrevencion(int idSedePrevencion, Long idSede, String nombre, String latitud, String longitud) {
        this.idSedePrevencion = idSedePrevencion;
        this.idSede = idSede;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getIdSedePrevencion() {
        return idSedePrevencion;
    }

    public void setIdSedePrevencion(int idSedePrevencion) {
        this.idSedePrevencion = idSedePrevencion;
    }

    public Long getIdSede() {
        return idSede;
    }

    public void setIdSede(Long idSede) {
        this.idSede = idSede;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

}
