package afinal.ricc.com.pe.proyectofinalcurso.application;

import android.app.Application;

import afinal.ricc.com.pe.proyectofinalcurso.rest.RestClient;

public class WSApplication extends Application {

    private static RestClient restAdmin;

    @Override
    public void onCreate() {
        super.onCreate();

        restAdmin = new RestClient("", "");


    }


    public static RestClient getRestAdmin() {
        return restAdmin;
    }


}