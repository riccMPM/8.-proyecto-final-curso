package afinal.ricc.com.pe.proyectofinalcurso.viewmodel;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import afinal.ricc.com.pe.proyectofinalcurso.model.Sede;


/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class SedeViewModel extends AndroidViewModel {

    private SedeRepository mRepository;

    public SedeViewModel(Application application) {
        super(application);
        mRepository = new SedeRepository(application);

    }

    public LiveData<List<Sede>> getAllSedes() {

        return mRepository.getAllSedes();
    }


    public void insertSedes(List<Sede> lstSede) {

        mRepository.insertSedes(lstSede);
    }

    public void updateSedes(List<Sede> lstSede) {
        mRepository.updateSedes(lstSede);
    }



}