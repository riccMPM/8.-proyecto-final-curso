package afinal.ricc.com.pe.proyectofinalcurso.util;

/**
 * Created by Lenovo on 04/10/2018.
 */

public class Constants {

    /*Table*/
    public static final String NAME_DATABASE = "curtainAppDB";

    public static final String NAME_TABLE_SEDE = "sede";
    public static final String NAME_TABLE_SEDE_PREVENCION = "sedePrevencion";


    public static final int REST_TIMEOUT = 25;
    public static final String BASE_URL = "http://50.16.25.164:8080/aunacm_ws/";

    public static final String RESPONSE_OK_GET = "OK";

}
