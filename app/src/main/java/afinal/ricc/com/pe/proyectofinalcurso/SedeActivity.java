package afinal.ricc.com.pe.proyectofinalcurso;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import afinal.ricc.com.pe.proyectofinalcurso.model.Sede;


/**
 * Created by user on 04/10/2018.
 */

public class SedeActivity extends AppCompatActivity {

    TextView tviNombreSede, tviDescripcionSede;
    ImageView iviSede;
    Sede sede;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sede);
        tviNombreSede = findViewById(R.id.tviNombreSede);
        tviDescripcionSede = findViewById(R.id.tviDescripcionSede);
        iviSede = findViewById(R.id.iviSede);

        sede = (Sede) getIntent().getSerializableExtra("intentSede");


    }

    @Override
    protected void onResume() {
        super.onResume();
        tviNombreSede.setText(sede.getNombreSede());
        tviDescripcionSede.setText(sede.getDescripcionSede());

        Picasso.get()
                .load(sede.getUrlImagen())
                .placeholder(R.drawable.img_clinica)
                .error(R.drawable.img_clinica)
                .fit().centerCrop()
                .into(iviSede);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        finish();

    }

}

