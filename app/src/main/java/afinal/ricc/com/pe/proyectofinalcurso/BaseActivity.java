package afinal.ricc.com.pe.proyectofinalcurso;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import afinal.ricc.com.pe.proyectofinalcurso.application.WSApplication;
import afinal.ricc.com.pe.proyectofinalcurso.rest.ApiService;

public class BaseActivity extends AppCompatActivity {

    public ApiService apiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = WSApplication.getRestAdmin().getApiService();

    }

    protected boolean isConnected() {
        Boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null) {
            isConnected = networkInfo.isConnected();
        }

        return isConnected;
    }
}