package afinal.ricc.com.pe.proyectofinalcurso.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import afinal.ricc.com.pe.proyectofinalcurso.util.Constants;

@Entity(tableName = Constants.NAME_TABLE_SEDE)
public class Sede implements Serializable {

    private static final long serialVersionUID = 87996564786747166L;

    @PrimaryKey()
    private int idSede;

    private Long idPadre;
    private String nombreSede;
    private String direccionSede;
    private String descripcionSede;
    private String numeroSede;
    private String urlImagen;
    private String urlSede;
    private Boolean activo;
    private Boolean esOncoSalud;
    private String color;
    private String latitud;
    private String longitud;
    private String urlReserva;
    private Boolean esCentroPrevencionTratamiento;
    private Boolean esReservaTelefonica;
    private Integer idImagen;
    private Boolean esCertificadoACI;
    private String distrito;
    private Boolean tieneResultadoMedico;
    private String urlLaboratorioMedico;


    public Sede() {
    }


    public Sede(int idSede, Long idPadre, String nombreSede, String direccionSede, String descripcionSede, String numeroSede, String urlImagen, String urlSede, Boolean activo, Boolean esOncoSalud, String color, String latitud, String longitud, String urlReserva, Boolean esCentroPrevencionTratamiento, Boolean esReservaTelefonica, Integer idImagen, Boolean esCertificadoACI, String distrito, Boolean tieneResultadoMedico, String urlLaboratorioMedico) {
        this.idSede = idSede;
        this.idPadre = idPadre;
        this.nombreSede = nombreSede;
        this.direccionSede = direccionSede;
        this.descripcionSede = descripcionSede;
        this.numeroSede = numeroSede;
        this.urlImagen = urlImagen;
        this.urlSede = urlSede;
        this.activo = activo;
        this.esOncoSalud = esOncoSalud;
        this.color = color;
        this.latitud = latitud;
        this.longitud = longitud;
        this.urlReserva = urlReserva;
        this.esCentroPrevencionTratamiento = esCentroPrevencionTratamiento;
        this.esReservaTelefonica = esReservaTelefonica;
        this.idImagen = idImagen;
        this.esCertificadoACI = esCertificadoACI;
        this.distrito = distrito;
        this.tieneResultadoMedico = tieneResultadoMedico;
        this.urlLaboratorioMedico = urlLaboratorioMedico;
    }

    public int getIdSede() {
        return idSede;
    }

    public void setIdSede(int idSede) {
        this.idSede = idSede;
    }

    public Long getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Long idPadre) {
        this.idPadre = idPadre;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getDireccionSede() {
        return direccionSede;
    }

    public void setDireccionSede(String direccionSede) {
        this.direccionSede = direccionSede;
    }

    public String getDescripcionSede() {
        return descripcionSede;
    }

    public void setDescripcionSede(String descripcionSede) {
        this.descripcionSede = descripcionSede;
    }

    public String getNumeroSede() {
        return numeroSede;
    }

    public void setNumeroSede(String numeroSede) {
        this.numeroSede = numeroSede;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getUrlSede() {
        return urlSede;
    }

    public void setUrlSede(String urlSede) {
        this.urlSede = urlSede;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getEsOncoSalud() {
        return esOncoSalud;
    }

    public void setEsOncoSalud(Boolean esOncoSalud) {
        this.esOncoSalud = esOncoSalud;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getUrlReserva() {
        return urlReserva;
    }

    public void setUrlReserva(String urlReserva) {
        this.urlReserva = urlReserva;
    }

    public Boolean getEsCentroPrevencionTratamiento() {
        return esCentroPrevencionTratamiento;
    }

    public void setEsCentroPrevencionTratamiento(Boolean esCentroPrevencionTratamiento) {
        this.esCentroPrevencionTratamiento = esCentroPrevencionTratamiento;
    }

    public Boolean getEsReservaTelefonica() {
        return esReservaTelefonica;
    }

    public void setEsReservaTelefonica(Boolean esReservaTelefonica) {
        this.esReservaTelefonica = esReservaTelefonica;
    }

    public Integer getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(Integer idImagen) {
        this.idImagen = idImagen;
    }

    public Boolean getEsCertificadoACI() {
        return esCertificadoACI;
    }

    public void setEsCertificadoACI(Boolean esCertificadoACI) {
        this.esCertificadoACI = esCertificadoACI;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public Boolean getTieneResultadoMedico() {
        return tieneResultadoMedico;
    }

    public void setTieneResultadoMedico(Boolean tieneResultadoMedico) {
        this.tieneResultadoMedico = tieneResultadoMedico;
    }

    public String getUrlLaboratorioMedico() {
        return urlLaboratorioMedico;
    }

    public void setUrlLaboratorioMedico(String urlLaboratorioMedico) {
        this.urlLaboratorioMedico = urlLaboratorioMedico;
    }


}
