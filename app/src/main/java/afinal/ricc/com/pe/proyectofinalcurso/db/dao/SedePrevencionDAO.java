package afinal.ricc.com.pe.proyectofinalcurso.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import afinal.ricc.com.pe.proyectofinalcurso.model.Sede;
import afinal.ricc.com.pe.proyectofinalcurso.model.SedePrevencion;
import afinal.ricc.com.pe.proyectofinalcurso.util.Constants;


@Dao
public interface SedePrevencionDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSedePrevencion(List<Sede> lstSedePrevencion);


    @Query("SELECT * FROM " + Constants.NAME_TABLE_SEDE_PREVENCION)
    LiveData<List<SedePrevencion>> listAll();


    @Query("DELETE FROM " + Constants.NAME_TABLE_SEDE_PREVENCION)
    void deleteAll();

    @Update
    void updateSedePrevencion(List<SedePrevencion> lstSedePrevencion);


}
